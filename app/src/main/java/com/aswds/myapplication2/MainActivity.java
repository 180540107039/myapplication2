package com.aswds.myapplication2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText etFirstName, etLastName;
    Button btnSubmit;
    TextView tvDisply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActLastName);
        btnSubmit = findViewById(R.id.btnActSubmit);

        tvDisply = findViewById(R.id.tvActDisplay);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstName = etFirstName.getText().toString();
                String lastName = etLastName.getText().toString();
                String temp = "Mr. or Mrs." + firstName + " " + lastName;
                tvDisply.setText(temp);

            }
        });
    }
}